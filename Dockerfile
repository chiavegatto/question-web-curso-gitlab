FROM trion/ng-cli:10.2.0 as base

WORKDIR /usr/src/app

COPY package.json .

RUN npm install

COPY . .

RUN ng build

FROM nginx:alpine

COPY --from=base /usr/src/app/dist/question-web /usr/share/nginx/html
